package com.example.filesystem;

import com.example.filesystem.entities.Directory;
import com.example.filesystem.services.DirectoryService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
class ApplicationTests {

    @Autowired
    private DirectoryService directoryService;

    @Test
    public void getDirectories() {
        List<Directory> list = directoryService.getDirectories();
        Assert.assertEquals(list.size() , 4l);
    }

    @Test
    public void addDirectory() {
        Directory directory = new Directory();
        directory.setPath("/");
        directory = directoryService.addDirectory(directory);
        Assert.assertEquals(directory.getDirectories().size() , 26l);
    }

    @Test
    public void testSorting() {
        Set<Directory> setTest = new HashSet<>();
        setTest.add(new Directory("F4_00127.pdf" , false));
        setTest.add(new Directory("function.cpp" , false));
        setTest.add(new Directory("innerTemp" , true));
        setTest.add(new Directory("f4_99.JPG" , false));
        setTest.add(new Directory("f0008.doc" , false));
        setTest.add(new Directory("F1.txt" , false));
        setTest.add(new Directory("X-FILES" , true));
        setTest.add(new Directory("f.txt" , false));

        List<Directory> correct = new ArrayList<>();
        correct.add(new Directory("innerTemp" , true));
        correct.add(new Directory("X-FILES" , true));
        correct.add(new Directory("f.txt" , false));
        correct.add(new Directory("F1.txt" , false));
        correct.add(new Directory("f4_99.JPG" , false));
        correct.add(new Directory("F4_00127.pdf" , false));
        correct.add(new Directory("f0008.doc" , false));
        correct.add(new Directory("function.cpp" , false));

        List<Directory> list = directoryService.sortDirectories(setTest);

        Assert.assertArrayEquals(list.toArray() , correct.toArray());
    }

}
