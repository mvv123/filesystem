CREATE DATABASE filesystem;

CREATE SEQUENCE directory_unique_id_seq;

CREATE TABLE directory (
    unique_id bigint NOT NULL DEFAULT nextval('directory_unique_id_seq'::regclass) PRIMARY KEY,
    time timestamp without time zone NOT NULL,
    path character varying NOT NULL,
    is_directory boolean NOT NULL,
    size bigint,
    parent_id bigint
);