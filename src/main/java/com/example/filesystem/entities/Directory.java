package com.example.filesystem.entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "directory")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Directory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "directory_gen")
    @SequenceGenerator(name = "directory_gen" , sequenceName = "directory_unique_id_seq" , allocationSize = 1)
    @Column(name = "unique_id")
    private Long uniqueId;

    @Column(name = "time")
    private Timestamp time;

    @Column(name = "path")
    @NonNull
    private String path;

    @Column(name = "is_directory")
    @NonNull
    private boolean isDirectory;

    @Column(name = "size")
    private Long size;

    @OneToMany(mappedBy = "parent" , fetch = FetchType.EAGER , cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private Set<Directory> directories;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id")
    @EqualsAndHashCode.Exclude
    private Directory parent;

    @Transient
    @EqualsAndHashCode.Exclude
    private long directoriesCount;

    @Transient
    @EqualsAndHashCode.Exclude
    private long filesCount;

    @Transient
    @EqualsAndHashCode.Exclude
    private long totalSize;

    @PostLoad
    private void count() {
        for(Directory directory : directories) {
            if(directory.isDirectory()) directoriesCount++;
            else {
                filesCount++;
                totalSize += directory.getSize();
            }
        }
    }

}
