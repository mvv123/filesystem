package com.example.filesystem.views;

import com.example.filesystem.entities.Directory;
import com.example.filesystem.services.DirectoryService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import org.springframework.core.env.Environment;

import java.util.Objects;

@Route("")
@CssImport(value = "./styles/grid.css" , themeFor = "vaadin-grid")
@CssImport(value = "./styles/text_field.css" , themeFor = "vaadin-text-field")
@CssImport(value = "./styles/main.css")
public class MainView extends VerticalLayout {

    private final DirectoryService directoryService;

    private final Environment environment;

    public MainView(DirectoryService directoryService , Environment environment) {
        this.directoryService = directoryService;
        this.environment = environment;
        setView();
    }

    private void setView() {
        Grid<Directory> grid = new Grid<>();
        grid.setItems(directoryService.getDirectories());
        grid.setHeightByRows(true);
        grid.setSizeFull();
        grid.setSelectionMode(Grid.SelectionMode.NONE);
        grid.addColumn(directory -> directoryService.convertTimestampToHuman(directory.getTime()))
                .setHeader(environment.getProperty("column.timestamp"))
                .setAutoWidth(true);
        grid.addColumn(Directory::getPath)
                .setHeader(environment.getProperty("column.directory"))
                .setAutoWidth(true);
        grid.addColumn(Directory::getDirectoriesCount)
                .setHeader(environment.getProperty("column.directories"));
        grid.addColumn(Directory::getFilesCount)
                .setHeader(environment.getProperty("column.files"));
        grid.addColumn(directory -> directoryService.convertByteToHuman(directory.getTotalSize()))
                .setHeader(environment.getProperty("column.totalsize"))
                .setAutoWidth(true);
        grid.addComponentColumn(this::addDirectoryButton)
                .setAutoWidth(true);

        TextField newDirectory = new TextField("" , environment.getProperty("field.placeholder"));
        newDirectory.setRequiredIndicatorVisible(true);
        newDirectory.addClassName("field");

        Binder<Directory> binder = new Binder<>();
        binder.forField(newDirectory)
                .asRequired(environment.getProperty("field.require"))
                .withValidator(value -> value.matches("[A-Za-zА-Яа-я0-9\\/\\_\\-\\.\\s]+") , environment.getProperty("field.matches.message"))
                .bind(Directory::getPath , Directory::setPath);
        binder.setBean(new Directory());

        Button createDirectory = new Button(environment.getProperty("button.directory.name") , buttonClickEvent -> {
            if(binder.validate().isOk()) {
                if(!Objects.isNull(directoryService.addDirectory(binder.getBean())))
                    grid.setItems(directoryService.getDirectories());
                else
                    getDialog(false , new Span(environment.getProperty("error.db"))).open();
                binder.setBean(new Directory());
            }
        });
        createDirectory.addClassName("create_btn");

        Span label = new Span(environment.getProperty("field.name"));
        label.addClassName("title");

        HorizontalLayout horizontalLayout = new HorizontalLayout(label , newDirectory , createDirectory);
        horizontalLayout.setAlignItems(Alignment.START);
        horizontalLayout.addClassName("wrapper");

        setHeightFull();
        add(new H2(environment.getProperty("header")) , horizontalLayout , new H3(environment.getProperty("title")) , grid);
    }

    private Button addDirectoryButton(Directory directory) {
        Button button = new Button(environment.getProperty("button.file.name"));
        button.addClassName("dialog_btn");
        button.addClickListener(e -> {
            Grid<Directory> grid = new Grid<>();
            grid.setItems(directoryService.sortDirectories(directory.getDirectories()));
            grid.addColumn(Directory::getPath)
                    .setHeader(environment.getProperty("column.file"));
            grid.addColumn(dir -> Objects.isNull(dir.getSize()) ? environment.getProperty("column.default") : directoryService.convertByteToHuman(dir.getSize()))
                    .setHeader(environment.getProperty("column.size"));
            grid.setHeightByRows(true);

            getDialog(true , new H4(directory.getPath() + " " + directoryService.convertTimestampToHuman(directory.getTime())) , grid).open();
        });
        return button;
    }

    private Dialog getDialog(boolean isFull, Component... components) {
        Dialog dialog = new Dialog();
        if(isFull) dialog.setWidthFull();
        for(Component component : components)
            dialog.add(component);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button close = new Button(environment.getProperty("button.close.name") , buttonClickEvent -> dialog.close());
        close.addClassName("close_btn");
        horizontalLayout.add(close);
        horizontalLayout.setJustifyContentMode(JustifyContentMode.END);
        dialog.add(horizontalLayout);
        return dialog;
    }

}
