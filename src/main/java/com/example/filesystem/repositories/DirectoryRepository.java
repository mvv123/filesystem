package com.example.filesystem.repositories;

import com.example.filesystem.entities.Directory;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DirectoryRepository extends JpaRepository<Directory , Long> {

    List<Directory> findAllByParentIsNull(Sort sort);

}
