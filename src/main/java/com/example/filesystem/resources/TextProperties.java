package com.example.filesystem.resources;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:/properties/text.properties")
public class TextProperties {}