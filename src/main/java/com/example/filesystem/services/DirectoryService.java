package com.example.filesystem.services;

import com.example.filesystem.entities.Directory;
import com.example.filesystem.repositories.DirectoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.File;
import java.sql.Timestamp;
import java.text.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DirectoryService {

    private final DirectoryRepository directoryRepository;

    public List<Directory> getDirectories() {
        return directoryRepository.findAllByParentIsNull(Sort.by(Sort.Direction.DESC, "uniqueId"));
    }

    public Directory addDirectory(Directory directory) {
        File root = new File(directory.getPath());
        if(root.exists() && Objects.nonNull(root.listFiles())) {
            Set<Directory> directories = new HashSet<>();
            for(File file : root.listFiles()) {
                Directory dir = new Directory();
                dir.setPath(file.getName());
                dir.setTime(new Timestamp(System.currentTimeMillis()));
                dir.setDirectory(file.isDirectory());
                if (file.isFile()) dir.setSize(file.length());
                dir.setParent(directory);
                directories.add(dir);
            }
            directory.setDirectories(directories);
            directory.setTime(new Timestamp(System.currentTimeMillis()));
            directory.setDirectory(root.isDirectory());
            return directoryRepository.save(directory);
        }
        return null;
    }

    public List<Directory> sortDirectories(Set<Directory> directories) {
        return directories.stream()
                .sorted(Comparator.comparing(Directory::isDirectory).reversed()
                .thenComparing(Directory::getPath , getCollator(directories)))
                .collect(Collectors.toList());
    }

    public String convertByteToHuman(long value) {
        if(value < 1024) return value + " b";
        int zeros = (63 - Long.numberOfLeadingZeros(value)) / 10;
        return String.format("%.1f %sb", (double) value / (1L << (zeros * 10)) , " KMGTPE".charAt(zeros));
    }

    public String convertTimestampToHuman(Timestamp time) {
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return formatter.format(time);
    }

    private RuleBasedCollator getCollator(Set<Directory> directories) {
        RuleBasedCollator baseRules = (RuleBasedCollator) Collator.getInstance();

        List<String> list = new ArrayList<>();

        directories.forEach(directory -> {
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(directory.getPath());
            while(matcher.find()) {
                String number = directory.getPath().substring(matcher.start() , matcher.end());
                if(!list.contains(number)) list.add(number);
            }
        });

        String addRules = list.stream()
                .sorted(Comparator.comparingInt(Integer::parseInt))
                .collect(Collectors.joining(" < "));

        try {
            if(list.size() == 0) return baseRules;
            else return new RuleBasedCollator(baseRules.getRules() + " & " + addRules);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return baseRules;
    }

}
