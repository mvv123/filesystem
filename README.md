# Тестовое задание #

Тестовое задание для СПб ИАЦ.

### Технологии ###

1. Spring-boot
2. Hibernate
3. Vaadin
4. PostgreSQL
5. Maven

### Запуск ###

1. Создать БД и сущности в PostgreSQL. (/src/main/resources/sql/script.sql)
2. mvn clean package -Dmaven.test.skip=true spring-boot:run или запуск статического метода в Application.class
3. После получения сообщения: "Started webpack-dev-server.", можно переходить на http://localhost:8080/